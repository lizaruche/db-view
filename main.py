from flask import Flask, render_template, request, redirect, url_for, session
from functools import wraps
import os
from db_queries import (get_id_by_username,
                        get_purchases,
                        pay_purchases,
                        get_dishes,
                        get_rests,
                        get_discounts,
                        add_purchase,
                        del_discount,
                        add_discount,
                        get_issue_categories,
                        new_issue)

app = Flask(__name__)
app.config['SECRET_KEY'] = os.environ.get('SECRETE_KEY_FL')


def login_required(func):
    @wraps(func)
    def secure_function(*args, **kwargs):
        if 'username' not in session:
            return redirect(url_for("login"))
        return func(*args, **kwargs)

    return secure_function


@app.route('/', methods={'GET', 'POST'})
@login_required
def index():
    if request.method == 'POST':
        checklist = request.form.getlist('purchases')  # get the list of active checkbox purchases
        if len(checklist) != 0:
            pay_purchases(checklist)
    if request.method == 'GET' and 'username' in session:
        purchases = get_purchases(session['username'])
    return render_template('debt.html',
                           name=session['username'],
                           active=['active', 'notactive', 'notactive'],
                           purchases=get_purchases(session['username']))


@app.route('/new_purchase', methods={'GET', 'POST'})
@login_required
def new_purchase():
    if request.method == 'POST':
        username_eat = get_id_by_username(request.form['username_eat'])
        sum_price = int(request.form['count']) * float(request.form['dish'])
        date = request.form['date']
        disc = request.form.get('disc')

        add_purchase(session["username_id"],
                     username_eat,
                     disc,
                     date,
                     sum_price)
        return redirect(url_for('index'))
    if request.method == 'GET':

        return render_template('new_purchase.html',
                               name=session['username'],
                               active=['active', 'notactive', 'notactive'],
                               restaurants=get_rests(),
                               dishes=get_dishes(),
                               discounts=get_discounts(session["username_id"]))


@app.route('/help', methods={'GET', 'POST'})
@login_required
def help():
    if request.method == 'POST':
        category = request.form['issue_category']
        text = request.form['issue_text']

        new_issue(category, session['username_id'], text)
        return redirect(url_for('index'))
    return render_template('help.html',
                           name=session['username'],
                           active=['notactive', 'notactive', 'active'],
                           categories=get_issue_categories())


@app.route('/login', methods={'GET', 'POST'})
def login():
    if request.method == 'POST':
        session["username"] = request.form['username']
        session["username_id"] = get_id_by_username(session["username"])
        session.modified = True
        return redirect(url_for('index'))
    return render_template('login.html')


@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('login'))


@app.route('/discounts', methods={'GET', 'POST'})
@login_required
def discounts():
    if request.method == 'POST':
        checklist = request.form.getlist('discounts')
        if len(checklist) != 0:
            del_discount(checklist)
    return render_template('discounts.html',
                           name=session['username'],
                           active=['notactive', 'active', 'notactive'],
                           discounts=get_discounts(session['username_id']))


@app.route('/new_discount', methods={'GET', 'POST'})
@login_required
def new_discount():
    if request.method == 'POST':
        restourant = request.form['restaurant']
        procent = request.form['procent']

        add_discount(session['username_id'], restourant, procent)
        return redirect(url_for('discounts'))
    return render_template('new_discount.html',
                           name=session['username'],
                           active=['notactive', 'active', 'notactive'],
                           restaurants=get_rests())


if __name__ == "__main__":
   app.run(host='0.0.0.0', port=8084, debug=True)

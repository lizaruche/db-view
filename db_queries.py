import psycopg2
from functools import wraps
import os

CONNECTION_DATA = {
        "user": os.environ.get('DB_USER'),
        "password": os.environ.get('DB_USER_PASSWORD'),
        "host": os.environ.get('DB_HOST'),
        "port": os.environ.get('DB_PORT'),
        "database": os.environ.get('DB')
        }


def handling_connection(func):
    @wraps(func)
    def getting(*args, **kwargs):
        connection = psycopg2.connect(**CONNECTION_DATA)
        cursor = connection.cursor()
        res = func(*args, **kwargs, cursor=cursor)
        if connection:
            cursor.close()
            connection.close()
        return res

    return getting


@handling_connection
def get_id_by_username(username: str, cursor):

    query = 'select id from public."public.users" where name = %s'

    cursor.execute(query, [username])
    temp = cursor.fetchall()
    result = temp[0][0] if len(temp) != 0 else -1

    if result == -1:
        result = add_user(username)

    return result


@handling_connection
def add_user(username: str, cursor):
    query = 'insert into public."public.users"("name", "is_help_user") values (%s, false) returning id;'

    cursor.execute(query, [username])
    cursor.connection.commit()
    temp = cursor.fetchall()
    result = temp[0][0]
    
    return result


@handling_connection
def get_purchases(username: str, cursor):
    query = '''
    select pur.id, e.eater_name, pur.date, pur.sum - pur.sum * d.procent / 100 from public."public.purchases" as pur
    inner join public."public.users" as u on pur.user_pay = u.id
    inner join public."public.discounts" as d on pur.discount = d.id
    inner join (select u.id, u.name as eater_name from public."public.users" as u) as e on pur.user_eat = e.id
    where u.name = %s
    and pur.payed = false;
    '''

    cursor.execute(query, [username])
    res = cursor.fetchall()
    return res


@handling_connection
def pay_purchases(purchases_id: list[str], cursor):
    tup_id = tuple(purchases_id)
    query = '''
    update public."public.purchases" as pur
    set payed = true
    where pur.id in %s
    '''

    cursor.execute(query, [tup_id])
    cursor.connection.commit()
    pass


@handling_connection
def get_dishes(cursor):
    query = '''
    select d.price, r.name, d.name from public."public.dishes" as d
    inner join public."public.restaurants" as r on r.id =d.restaurant
    where r.existence = true
    '''

    cursor.execute(query)
    res = cursor.fetchall()
    return res


@handling_connection
def get_rests(cursor):
    query = '''
    select r.id, r.name from public."public.restaurants" as r
    where r.existence = true
    '''

    cursor.execute(query)
    res = cursor.fetchall()
    return res


@handling_connection
def get_discounts(user_id: int, cursor):
    query = '''
    select d.id, r.name, d.procent from public."public.discounts" as d
    inner join public."public.restaurants" as r on r.id =d.restaurant
    where d.owner = %s
    and d.exist = true
    '''

    cursor.execute(query, [user_id])
    res = cursor.fetchall()
    return res


@handling_connection
def add_purchase(user_pay_id: int, username_eat: int, discount: int, date: int, sum: int, cursor):
    tmp = (user_pay_id, username_eat, discount, date, sum, False)
    query = '''
    insert into public."public.purchases" as pur("user_pay", "user_eat", "discount", "date", "sum", "payed") values
    %s
    '''

    cursor.execute(query, [tmp])
    cursor.connection.commit()
    pass


@handling_connection
def del_discount(discount_id: list[int], cursor):
    tmp = tuple(discount_id)
    query = '''
    update public."public.discounts" as d
    set exist = false
    where d.id = %s
    '''

    cursor.execute(query, [tmp])
    cursor.connection.commit()
    pass


@handling_connection
def add_discount(owner_id: int, rest_id: int, procent_num: int, cursor):
    query = '''
    insert into public."public.discounts"("owner", "restaurant", "procent", "exist") values %s
    '''

    cursor.execute(query, [(owner_id, rest_id, procent_num, True)])
    cursor.connection.commit()
    pass


@handling_connection
def get_issue_categories(cursor):
    query = '''
    select * from public."public.issue_categories" 
    '''
    cursor.execute(query)
    return cursor.fetchall()


@handling_connection
def new_issue(category_id: int, owner_id: int, text: str, cursor):
    query = '''
    insert into public."public.issues"("category", "owner", "user_helper", "text") values %s
    '''

    cursor.execute(query, [(category_id, owner_id, 1, text)])
    cursor.connection.commit()
    pass

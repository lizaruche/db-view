FROM python:3.10.8

WORKDIR /view

COPY ./requirements.txt /view/requirements.txt

ENV DB_USER root
ENV DB_USER_PASSWORD root
ENV DB_HOST pg_container
ENV DB_PORT 5432
ENV DB divBill_db
ENV SECRETE_KEY_FL f08efd58063a97f23dd9b730fd43d68cf3b22d0d635533ec

RUN pip install --no-cache-dir --upgrade -r /view/requirements.txt

COPY ./templates /view/templates
COPY ./main.py /view/main.py
COPY ./db_queries.py /view/db_queries.py
EXPOSE 8084

CMD ["python3", "main.py"]
